This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation

1. Type 'npm install' in the terminal to install the packages / dependencies.
2. Type 'npm start' to run the project in the browser. (http://Localhost:3000)
3. Stories landing page: http://Localhost:3000
4. Comments page: http://localhost:3000/storycomments/XXXXXX
   Please replace 'XXXXXX' with Story ID. Clicking on the stories listed on the home page will lead to the comments for the respective story.

### `Project file structure`

1. Components / Containers folder to accomodate functional and class based components.
2. React Browser Router has been implemented for navigation.

### `Scope of Improvements`

1. React hooks or Reducer can be used once the application grows.
2. PropTypes have been used but, Typescript can be used as well for stricty type checks.
3. For CSS / UI - StyledComponents / SASS can further be used depending upon the layout needs.
4. Client / Server implementation using Node - Express server can be implemented as well considering future prospects.
5. GraphQL can replace Rest API if needed.
6. Utility function can be kept separately in the different folder structure and we can import in the class / functional components. For ex: date sorting utility in this case. I've sorted stories based on the dates as per descending order. Filter can be used on the top as well to change the sorting order / set stories as per alphabetic order, etc.
