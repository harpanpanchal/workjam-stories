import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import Stories from './containers/Stories/Stories';

import StoryComments from './containers/StoryComments/StoryComments';

class App extends Component {
  render() {
return (
<Router>
<Switch>
<Route exact path="/storycomments/:id" component={StoryComments} />
<Route exact path="/" component={Stories} />
</Switch>
</Router>
);
}
}

export default App;