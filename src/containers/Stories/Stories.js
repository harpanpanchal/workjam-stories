import React, { Component } from 'react';
import axios from '../../axios';
import {Link} from 'react-router-dom';
import Story from '../../components/Story/Story';
import "./Stories.css";

class Stories extends Component {
    state = {
        posts: [],
        selectedPostId: null,
        error: false,
        errorMessage: {
            loading: 'Loading !!!!',
            connectionIssue: 'Sorry, there seems some network connectivity or backend issue. Please try again later.'   
        }
    }
    componentDidMount() {
        axios.get('/v0/topstories.json')
            .then((response) => {
                if (response.statusText === 'OK' && response.status === 200) {
                    const posts = response.data.slice(0, 10);
                    const updatedPosts = posts.map(post => {          
                        return {
                            id: post
                        }
                    });
                    updatedPosts.map((post) => {
                        axios.get(`/v0/item/${post.id}.json`)
                            .then((response) => {
                                this.setState({
                                    ...this.state, posts: [...this.state.posts, {
                                        id: response.data.id,
                                        title: response.data.title,
                                        time: response.data.time
                                    }]
                                })
                            })                    
                            return {
                                id: response.data.id,
                                title: response.data.title,
                                time: response.data.time
                            }
                    });
                }
                else {
                    this.setState({
                        ...this.state, error: true
                    })
                }
                

                
      })
        }
    render() {
        const { error, posts, errorMessage } = this.state;
        const {loading, connectionIssue } = errorMessage;
        let renderedPosts = '';
        if (error === true) {
            renderedPosts = <p className='centeredText'>{connectionIssue}</p>;
        }
        if (!posts || posts.length === 0) {
            renderedPosts = <p className='centeredText'>{loading}</p>;
        }
        if (posts && posts.length > 0) {
            const dateWiseSorting = posts.sort(function(a, b) {
                const dateA = new Date(a.time), dateB = new Date(b.time);
                return dateB - dateA;
            });

            renderedPosts = dateWiseSorting.map(({id, time,title}) => {
                const fetchedDate = new Date(time * 1000);
                
                return (<div className="ui segment" key={id} ><Link to={`/storycomments/${id}`}><Story title={title} time={fetchedDate.toUTCString()} /></Link></div>)
            });
        }
        return (<div className="ui container"><div className="ui hidden divider"></div><h1 className="ui header">Latest Stories</h1> <div className="sub header">Following are the top 10 amazing stories !!!!</div><div className="ui segments">{renderedPosts}</div></div>);
    }
}

export default Stories;