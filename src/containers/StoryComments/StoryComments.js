import React, {Component} from 'react';
import axios from 'axios';
import ReactHtmlParser from 'react-html-parser';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import './StoryComment.css';

class StoryComments extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loadedPost: [],
            storyId: this.props.match.params.id || '',
            error: false,
            errorMessage: {
                loading: 'Loading !!!!',
                incorrectId: 'Sorry, it seems either incorrect ID or no comments associated with the story. Please try again.'
            }
        }
    }
    componentDidMount() {
        const { storyId, loadedPost } = this.state;
        if (storyId && storyId !== '' && storyId !== null) {
            if (!loadedPost || (loadedPost && loadedPost.id !== storyId)) {
                axios.get('v0/item/' + storyId + '.json')
                    .then(response => {

                        if (response.data !== null && response.data.kids !== undefined) {
                            const storyComments = response.data.kids.slice(0, 20);
                            const updatedPosts = storyComments.map(storyComment => {
                                return {
                                    id: storyComment
                                }
                            });
                            updatedPosts.map((storyComment) => {
                                axios.get('v0/item/' + storyComment.id + '.json')
                                    .then(response => {
                                        this.setState({
                                            ...this.state, loadedPost: [...this.state.loadedPost, {
                                                id: response.data.id,
                                                text: response.data.text,
                                                time: response.data.time
                                            }]
                                        })
                                         
                                    });
                                return {
                                    id: storyComment.id,
                                    text: response.data.text,
                                    time: response.data.time
                                }
                            });
                        }
                        else {
                            this.setState({
                                ...this.state, error: true
                            })
                        }
                    });
            }
        }
    }

    render() {
        let post = '';
        const { loadedPost, storyId, error, errorMessage } = this.state;
        const { loading, incorrectId } = errorMessage;

        

        if (error === true) {
            post = <p className='centeredText'>{incorrectId}</p>;
        }
        if (storyId && error === false && (!loadedPost || loadedPost.length === 0)) {
            post = <p className='centeredText'>{loading}</p>;
        }
        if (loadedPost && loadedPost.length > 0) {
            const dateWiseSorting = loadedPost.sort(function (a, b) {
                const dateA = new Date(a.time), dateB = new Date(b.time);
                return dateB - dateA;
            });
            
            const renderedPosts = dateWiseSorting.map(({ id, time, text }) => {
                const fetchedDate = new Date(time * 1000);
                return (<li key={id}>{ReactHtmlParser(text)}<br /><span className='dateStyle'>{fetchedDate.toUTCString()}</span></li>)
            });
            post = <ol className="ui segment">{renderedPosts}</ol>
        };
        

        return (<div className="ui container">
            <div className="ui hidden divider"></div>
            <Link to='/'>Back</Link>
            <div className="ui hidden divider"></div>
            <div className='ui container'>{post}</div></div>)

    }
};
StoryComments.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
          id: PropTypes.isRequired
        })
    })
}

export default StoryComments;