import React from 'react';
import PropTypes from 'prop-types';

const Story = ({ title, time }) => <>
  <span className='revealList'>{title}</span> &ndash;
  <span className='right'><i className="calendar alternate outline icon"></i>{time}</span>
</>;

Story.propTypes = {
  title: PropTypes.string,
  time: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date),
    PropTypes.number
  ]),
  }

export default Story;